<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyControl extends Controller
{
    //
    public function getShow(){
        return view('show');
    }

    public function getAdd(){
        return view('add');
    }

    public function getIndex(){
        return view('index');
    }

    public function insert() {
        $demo = new App\demo();
        $demo->string1 = " data1";
        $demo->string2 = " data2";
        $demo->save();
    }
}
