<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>MAIN | @yield('title')</title>
        <link rel="stylesheet" href="">
    </head>
    <body>
        <h1>Main</h1>
        @yield('main')
    </body>
</html>