<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// đường dẫn
Route::get('/hello/{id}', function ($id) {
    echo 'hello ' .  $id;
});

// đường dẫn 2 (có id hoặc không vẫn cứ là oke :v )
Route::get('/hello/{id?}', function ($id = 'null') {
    echo 'hello ' .  $id;
});

// tính tổng cơ bản
Route::get('/sum/{int1}/{int2}', function ($int1,$int2) {
    $tong = $int1 + $int2;
    return $tong;
});

// prefix : tiền tố ,.. tạo 1 mảng các route
Route :: group(['prefix' => 'admin'],function(){
    Route :: get('home',function(){
       echo 'home';
    });
    // group trong group
    Route :: group(['prefix' => 'product'], function(){
        Route :: get('add',function(){
            echo 'add';
        });
    });
});

// control tên 'controller' + '@' + 'phương thức' :
// chuyển route sang controller
Route :: get('/control','FirstController@getController');

//control 2 có tham số
Route :: get('/control/{x}/{y}','FirstController@getController2');

//get view
Route :: get('view','FirstController@getView');

//get view
Route :: get('view1','FirstController@getView1');

//get view
Route :: get('view2','FirstController@getView2');

//get view
Route :: get('home','FirstController@getHome');

//get view
Route :: get('demo','FirstController@getDemo');
